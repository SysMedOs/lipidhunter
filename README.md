# LipidHunter will be hosted on GitHub
# Please check our latest updates on [https://github.com/SysMedOs/lipidhunter](https://github.com/SysMedOs/lipidhunter)
## LipidHunter2 is ready for beta test
## *Please write email to zhixu.ni@uni-leipzig.de*

### Special Feature for TG & DG

+ ID based both on FA neutral losses & fragments
+ Accurate ID of isomeric species
+ Correction for all identified FA
+ Correction for the fragment intensity of multiply identified  FAs
+ Check for cross-contamination signals e.g. PL fragments

### Other Major Feature Updates

+ Multiprocessing
+ Batch mode
+ 10 times faster processing speed
+ Command line mode
+ KNIME workflow integration
+ Multiple vendor support
+ Improved output style
+ Simplified configuration
+ View run parameters in report


# LipidHunter instructions (source code version) #
## This repository contains only the original version of LipidHunter

![crossplatform_screenshot.png](https://bitbucket.org/repo/oGzkj4/images/622833109-crossplatform_screenshot.png)

This repository contains the source code of LipidHunter.

LipidHunter Windows .exe executable version can be found here

* https://bitbucket.org/SysMedOs/lipidhunter_exe

    + [LipidHunter for Windows 10 64bit ( `.zip` file around 310 MB)](https://bitbucket.org/SysMedOs/lipidhunter_exe/downloads/LipidHunter_Win10_64bit.zip)

    + [LipidHunter for Windows 7,8 and 8.1 64bit ( `.zip` file around 240 MB)](https://bitbucket.org/SysMedOs/lipidhunter_exe/downloads/LipidHunter_Win7-8_64bit.zip) 


** Please read the following instructions before you start to run LipidHunter. **

### Instructions ###

* [How to install LipidHunter from source code](#markdown-header-how-to-install-lipidhunter-from-source-code)
* [License](#markdown-header-license)
* [A step by step tutorial](https://bitbucket.org/SysMedOs/lipidhunter/wiki/Home)
* [Q&A](#markdown-header-further-questions)
* [Fundings](#markdown-header-fundings)



### How to install LipidHunter from source code ###
* Download the LipidHunter as zip file for your system

    + [Download LipidHunter source Code](https://bitbucket.org/SysMedOs/lipidhunter/downloads/?tab=tags) Please notice the date and version of LipidHunter source code.
    + Professional users can use `git` to clone the whole repository, please make sure that you switched to the correct branch.



* Rename the downloaded file to `LipidHunter.zip`
* Unzip `LipidHunter.zip` file to any folder.
* Downloaded LipidHunter test spectra files: [LipidHunter_Test_mzML_File](https://bitbucket.org/SysMedOs/lipidhunter_exe/downloads/LipidHunter_TestFile.zip)
* Follow the link below for more information to install the program.

    + [LipidHunter source code installation instructions](https://bitbucket.org/SysMedOs/lipidhunter/wiki/Install%20the%20Source%20code)

* LipidHunter user guide: [Download LipidHunter user guide PDF](https://bitbucket.org/SysMedOs/lipidhunter_exe/downloads/LipidHunter_User_Guide.pdf)



* **Known issues/bugs**
     - The temporary solution for `Agilent` .d raw https://bitbucket.org/SysMedOs/lipidhunter/issues/1/agilent-d-files
     - The known issues with `pymzML` obo file problems. https://bitbucket.org/SysMedOs/lipidhunter/issues/2/pymzml-psi-msobo-file-issue
    
    In case you experienced any problems with running LipidHunter, please report an issue in the [issue tracker](https://bitbucket.org/SysMedOs/lipidhunter/issues) or contact us.

### License ###

+ LipidHunter is Dual-licensed
    * For academic and non-commercial use: `GPLv2 License`: 
    
        [The GNU General Public License version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

    * For commercial use: please contact the develop team by email.

+ Please cite our publication in an appropriate form.

     - [Ni, Zhixu, Georgia Angelidou, Mike Lange, Ralf Hoffmann, and Maria Fedorova. "LipidHunter identifies phospholipids by high-throughput processing of LC-MS and shotgun lipidomics datasets." Analytical Chemistry (2017).](http://pubs.acs.org/doi/10.1021/acs.analchem.7b01126)
    
        * DOI: [`10.1021/acs.analchem.7b01126`](http://pubs.acs.org/doi/10.1021/acs.analchem.7b01126)

### Further questions? ###

* Read our [wiki](https://bitbucket.org/SysMedOs/lipidhunter/wiki/Home)
* Report any issues here: [https://bitbucket.org/SysMedOs/lipidhunter/issues](https://bitbucket.org/SysMedOs/lipidhunter/issues)


### Fundings ###
We acknowledge all projects that supports the development of LipidHunter:

+ BMBF - Federal Ministry of Education and Research Germany:

    https://www.bmbf.de/en/

+ e:Med Systems Medicine Network:

    http://www.sys-med.de/en/

+ SysMedOS Project : 

    https://home.uni-leipzig.de/fedorova/sysmedos/